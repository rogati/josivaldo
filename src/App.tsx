import React, { useState } from 'react';

import Card from './components/Card/Card';
import ToggleSwitch from "./components/ToggleSwitch/ToggleSwitch";

import styles from './styles/global.module.scss';
interface ItCard {
  title: string;
  description: string;
  iconDescription: string;
  organization: string;
  amountItem: string;
  buttonName: string;
  textItem: string;
  locale: string;
}

function App() {
  const [cards, setCards] = useState([
    {
      title: "doação de materiais",
      description: "Cestas Básicas",
      organization: "Aldeia da Fraternidade",
      iconDescription: "box_healt.svg",
      amountItem: "120",
      buttonName: "Doar",
      textItem: "disponíveis",
      locale: "Porto Alegre, RS",
    },
    {
      title: "doação de materiais",
      description: "Arrecação de Alimentos para moradores de rua",
      organization: "Transforma Petrolina",
      iconDescription: "box_healt.svg",
      amountItem: "28",
      buttonName: "Doar",
      textItem: "disponíveis",
      locale: "Petrolina, PE",
    },
    {
      title: "vaga de voluntariado",
      description: "Assistente Social",
      organization: "AMA(Associação Mundo Azul)",
      iconDescription: "solidary.svg",
      amountItem: "36",
      buttonName: "Participar",
      textItem: "vagas disponíveis",
      locale: "Remota",
    },
    {
      title: "vaga de voluntariado",
      description: "Designer Gráfico",
      organization: "Mobiliza Já!",
      iconDescription: "solidary.svg",
      amountItem: "28",
      buttonName: "Participar",
      textItem: "horas disponíveis",
      locale: "Remota",
    },
    {
      title: "vaga de voluntariado",
      description: "Professor de Educação Física",
      organization: "CENTRO MARIA AUXILIADORA PRO MENOR - CEMAN",
      iconDescription: "solidary.svg",
      amountItem: "28",
      buttonName: "Participar",
      textItem: "horas disponíveis",
      locale: "Petrolina, PE",
    },
    {
      title: "campanha de arrecação",
      description: "Arrecação de Alimentos para moradores de rua",
      organization: "FACE Acontecer",
      iconDescription: "coins.svg",
      amountItem: "R$ 3.240",
      buttonName: "Contribuir",
      textItem: "valor esperado",
      locale: "Natal, RN",
    },
    {
      title: "vaga de voluntariado",
      description: "Designer Gráfico",
      organization: "Mobiliza Já!",
      iconDescription: "solidary.svg",
      amountItem: "28",
      buttonName: "Participar",
      textItem: "horas disponíveis",
      locale: "Remota",
    },
    {
      title: "campanha de arrecação",
      description: "Arrecação de Alimentos para moradores de rua",
      organization: "FACE Acontecer",
      iconDescription: "coins.svg",
      amountItem: "R$ 3.240",
      buttonName: "Contribuir",
      textItem: "valor esperado",
      locale: "Natal, RN",
    },
    {
      title: "doação de materiais",
      description: "Arrecação de Alimentos para moradores de rua",
      organization: "Transforma Petrolina",
      iconDescription: "box_healt.svg",
      amountItem: "28",
      buttonName: "Doar",
      textItem: "disponíveis",
      locale: "Petrolina, PE",
    },

  ]);

  return (
    <div className="App">
      <div className={styles.container}>
        <div className={styles.headerPage}>
          <h3 className={styles.titlePage}>Oportunidades em destaque</h3>
          <span className={styles.labelLocation}>
          </span>
          <ToggleSwitch />
        </div>
        <div className={styles.content}>
          <ul className={styles.listCard}>
            {cards.map(card => (
              <li className={styles.itemCard}>
                <Card card={card} />
              </li>
            ))}
          </ul>
        </div>
        <div className={styles.plusCards}>
          <button className={styles.outlineGreen}>Todas Oportunidades</button>
        </div>
      </div>
    </div>
  );
}

export default App;
