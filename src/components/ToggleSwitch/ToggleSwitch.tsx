import React, { useState } from "react";
import Switch from "react-switch";  

import styles from './styles.module.scss';

const ToggleSwitch = () => {
  const [checked, setChecked] = useState(false)

  return (
    <label className={styles.labelToggle}>
      <span className={styles.textToggle}>Geolocalização {checked ? "Ativada" : "Desativada"}</span>
        <Switch 
          checkedIcon={false}
          uncheckedIcon={false}
          checked={checked} 
          handleDiameter={13}
          offColor="#ccc"
          onColor="#21D170"
          height={20}
          width={34}
          onChange={(e) => {
            setChecked(e)
          }}
        />
    </label>
  )
}

export default ToggleSwitch;