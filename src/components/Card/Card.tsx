import React from "react";

import styles from './styles.module.scss';

import Button from '../Button/Button';

interface ItCard {
  title: string;
  description: string;
  organization: string;
  iconDescription: string;
  amountItem: string;
  textItem: string;
  buttonName: string;
  locale: string;
}

interface Props {
  card: ItCard;
}

const Card: React.FunctionComponent<Props> = ({ card }) => {
  return (
    <>
      <header className={styles.cardHeader}>{card.title}</header>
      
      <div className={styles.cardContent}>
        <div className={styles.cardBody}>
          <div className={styles.cardDescription}>
            <p className={styles.textDescription}>{card.description}</p>
            <img src={card.iconDescription} />
          </div>
          <div className={styles.textProject}>
            <a href= "#" className={styles.linkProject}>{card.organization}</a>
          </div>
        </div>

        <div className={styles.cardLinks}>
          <div className={styles.labelItems}>
            <span className={styles.amountItem}>{card.amountItem}</span>
            <p className={styles.textItem}>{card.textItem}</p>
          </div>
          <div className={styles.labelItems}>
            <Button name={card.buttonName} /> 
          </div>
        </div>
      </div>

      <footer className={styles.cardFooter}>
        <p className={styles.textFooter}>{card.locale}</p>
      </footer>
    </>
  );
}

export default Card;